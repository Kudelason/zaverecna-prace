fetch('http://localhost:8080/directors')
    .then(response => response.json())
    .then(directors => {
        const list = document.getElementById('directorsList');
        directors.forEach(director => {
            const item = document.createElement('li');
            const link = document.createElement('a');
            link.href = `director.html?id=${director.id}`;
            link.textContent = director.name;
            item.appendChild(link);
            list.appendChild(item);
        });
    })
    .catch(error => console.error('Error:', error));