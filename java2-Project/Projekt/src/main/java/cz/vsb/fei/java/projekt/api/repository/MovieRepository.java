package cz.vsb.fei.java.projekt.api.repository;

import cz.vsb.fei.java.projekt.api.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
