package cz.vsb.fei.java.projekt.api.controller;

import cz.vsb.fei.java.projekt.api.model.Movie;
import cz.vsb.fei.java.projekt.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/movies")  // This base path will apply to all methods
public class MovieController {

    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}") // Using PathVariable for better REST practices
    public ResponseEntity<Movie> getMovie(@PathVariable int id) {
        return movieService.getMovie(id)
                .map(ResponseEntity::ok) // If found, return 200 OK with the movie
                .orElseGet(() -> ResponseEntity.notFound().build()); // If not found, return 404 Not Found
    }
    @CrossOrigin(origins = "*")
    @GetMapping // No need for a specific path since we're using RequestMapping at the class level
    public List<Movie> getMovies() {
        return movieService.getMovies(); // Your service already handles the optional, no need for further checks
    }
}
