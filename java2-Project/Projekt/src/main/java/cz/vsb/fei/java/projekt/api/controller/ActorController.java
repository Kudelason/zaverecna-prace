package cz.vsb.fei.java.projekt.api.controller;

import cz.vsb.fei.java.projekt.api.model.Actor;
import cz.vsb.fei.java.projekt.api.model.Director;
import cz.vsb.fei.java.projekt.service.ActorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/actors")
public class ActorController {

    private final ActorService actorService;

    public ActorController(ActorService actorService) {
        this.actorService = actorService;
    }

    @CrossOrigin(origins = "*")
    @GetMapping
    public List<Actor> getAllActors() {
        return actorService.getAllActors();
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}") // Using PathVariable for better REST practices
    public ResponseEntity<Actor> getActor(@PathVariable int id) {
        return actorService.getActor(id)
                .map(ResponseEntity::ok) // If found, return 200 OK with the movie
                .orElseGet(() -> ResponseEntity.notFound().build()); // If not found, return 404 Not Found
    }
}

