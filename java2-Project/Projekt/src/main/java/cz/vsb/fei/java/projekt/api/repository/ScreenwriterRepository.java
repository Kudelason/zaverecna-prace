package cz.vsb.fei.java.projekt.api.repository;

import cz.vsb.fei.java.projekt.api.model.Screenwriter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScreenwriterRepository extends JpaRepository<Screenwriter, Integer> {
}
