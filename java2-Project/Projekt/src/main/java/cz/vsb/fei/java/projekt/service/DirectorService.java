package cz.vsb.fei.java.projekt.service;

import cz.vsb.fei.java.projekt.api.model.Director;
import cz.vsb.fei.java.projekt.api.model.Movie;
import cz.vsb.fei.java.projekt.api.repository.DirectorRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DirectorService {

    private final DirectorRepository directorRepository;

    public DirectorService(DirectorRepository directorRepository) {
        this.directorRepository = directorRepository;
    }

    public Director addDirector(Director director) {
        return directorRepository.save(director);
    }

    public List<Director> getAllDirectors() {
        return directorRepository.findAll();
    }

    @Transactional
    public Optional<Director> getDirector(int id) {
        Optional<Director> director = directorRepository.findById(id);
        return director;
    }
}
