package cz.vsb.fei.java.projekt.service;

import cz.vsb.fei.java.projekt.api.model.Director;
import cz.vsb.fei.java.projekt.api.model.Movie;
import cz.vsb.fei.java.projekt.api.model.Screenwriter;
import cz.vsb.fei.java.projekt.api.model.Actor;
import cz.vsb.fei.java.projekt.api.repository.ActorRepository;
import cz.vsb.fei.java.projekt.api.repository.DirectorRepository;
import cz.vsb.fei.java.projekt.api.repository.MovieRepository;
import cz.vsb.fei.java.projekt.api.repository.ScreenwriterRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class MovieService {
    private static final Logger log = LoggerFactory.getLogger(MovieService.class);
    private final MovieRepository movieRepository;
    private final DirectorRepository directorRepository;
    private final ScreenwriterRepository screenwriterRepository;
    private final ActorRepository actorRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository , DirectorRepository directorRepository, ScreenwriterRepository screenwriterRepository, ActorRepository actorRepository) {
        this.movieRepository = movieRepository;
        this.directorRepository = directorRepository;
        this.screenwriterRepository = screenwriterRepository;
        this.actorRepository = actorRepository;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void initMovies() {
        if (movieRepository.count() == 0) { // Only initialize if no data is present
            // Create directors
            Director nolan = new Director("Christopher Nolan", "British-American", "1970-07-30");
            Director wachowskis = new Director("Lana and Lilly Wachowski", "American", "1965-06-21");
            Director coppola = new Director("Francis Ford Coppola", "American", "1939-04-07");

            // Save directors
            directorRepository.saveAll(List.of(nolan, wachowskis, coppola));

            // Create screenwriters
            Screenwriter inceptionWriter = new Screenwriter("Jonathan Nolan", "British-American", "1976-06-06");
            Screenwriter metrixWriter = new Screenwriter("Lana and Lilly Wachowski", "American", "1967-12-29");
            Screenwriter interstellarWriter = new Screenwriter("Kip Thorne", "American", "1940-06-01");
            Screenwriter godfatherWriter = new Screenwriter("Mario Puzo", "American", "1920-10-15");

            screenwriterRepository.saveAll(List.of(inceptionWriter, metrixWriter, interstellarWriter,godfatherWriter));

            // Create movies with associated directors
            List<Movie> movies = List.of(
                    new Movie("Inception", "A thief who steals corporate secrets through the use of dream-sharing technology is given the inverse task of planting an idea into the mind of a CEO.", 2010, nolan, inceptionWriter),
                    new Movie("The Matrix", "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.", 1999, wachowskis, metrixWriter),
                    new Movie("Interstellar", "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.", 2014, nolan, interstellarWriter),
                    new Movie("The Godfather", "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.", 1972, coppola, godfatherWriter),
                    new Movie("The Godfather: Part II", "The early life and career of Vito Corleone in 1920s New York City is portrayed, while his son, Michael, expands and tightens his grip on the family crime syndicate.", 1974, coppola, godfatherWriter)
                    );

            // Create actors for Inception
            Actor dicaprio = new Actor("Leonardo DiCaprio", "1974-11-11");
            Actor levitt = new Actor("Joseph Gordon-Levitt", "1981-02-17");
            Actor page = new Actor("Ellen Page", "1987-02-21");

            // Create actors for The Matrix
            Actor reeves = new Actor("Keanu Reeves", "1964-09-02");
            Actor fishburne = new Actor("Laurence Fishburne", "1961-07-30");
            Actor moss = new Actor("Carrie-Anne Moss", "1967-08-21");

            // Create actors for Interstellar
            Actor mcconaughey = new Actor("Matthew McConaughey", "1969-11-04");
            Actor hathaway = new Actor("Anne Hathaway", "1982-11-12");
            Actor chastain = new Actor("Jessica Chastain", "1977-03-24");

            // Create actors for The Godfather
            Actor brando = new Actor("Marlon Brando", "1924-04-03");
            Actor pacino = new Actor("Al Pacino", "1940-04-25");
            Actor caan = new Actor("James Caan", "1940-03-26");

            // Create actors for The Godfather: Part II
            Actor deNiro = new Actor("Robert De Niro", "1943-08-17");
            Actor dianeKeaton = new Actor("Diane Keaton", "1946-01-05");
            Actor robertDuvall = new Actor("Robert Duvall", "1931-01-05");


            actorRepository.saveAll(List.of(dicaprio, levitt, page, reeves, fishburne, moss, mcconaughey, hathaway, chastain, brando, pacino, caan, deNiro, dianeKeaton, robertDuvall));

            //Update movies list
            movies.get(0).getActors().addAll(Set.of(dicaprio, levitt, page));
            movies.get(1).getActors().addAll(Set.of(reeves, fishburne, moss));
            movies.get(2).getActors().addAll(Set.of(mcconaughey, hathaway, chastain));
            movies.get(3).getActors().addAll(Set.of(brando, pacino, caan, robertDuvall));
            movies.get(4).getActors().addAll(Set.of(brando, pacino, caan, robertDuvall, deNiro, dianeKeaton));


            // Update director's movies lists
            nolan.getMovies().addAll(List.of(movies.get(0), movies.get(2))); // Inception and Interstellar
            wachowskis.getMovies().add(movies.get(1)); // The Matrix
            coppola.getMovies().add(movies.get(3)); // The Godfather
            coppola.getMovies().add(movies.get(4));


            // Update screenwriter's movies lists
            inceptionWriter.getMovies().add(movies.get(0));
            metrixWriter.getMovies().add(movies.get(1));
            interstellarWriter.getMovies().add(movies.get(2));
            godfatherWriter.getMovies().add(movies.get(3));
            godfatherWriter.getMovies().add(movies.get(4));

            // Save movies
            movieRepository.saveAll(movies);
        }
    }

    @Transactional
    public List<Movie> getMovies() {
        log.info("Fetching all movies");
        return movieRepository.findAll();
    }

    @Transactional
    public Optional<Movie> getMovie(int id) { // Ensure this matches the ID type in your entity
        log.info("Fetching movie with ID: {}", id);
        Optional<Movie> movie = movieRepository.findById(id);
        movie.ifPresent(m -> log.info("Found movie: {}", m.getActors()));
        return movie;
    }

    public Movie saveMovie(Movie movie) {
        return movieRepository.save(movie);
    }
}
