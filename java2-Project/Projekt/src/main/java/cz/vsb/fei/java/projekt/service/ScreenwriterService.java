package cz.vsb.fei.java.projekt.service;

import cz.vsb.fei.java.projekt.api.model.Director;
import cz.vsb.fei.java.projekt.api.model.Screenwriter;
import cz.vsb.fei.java.projekt.api.repository.ScreenwriterRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ScreenwriterService {
    private final ScreenwriterRepository screenwriterRepository;

    public ScreenwriterService(ScreenwriterRepository screenwriterRepository) {
        this.screenwriterRepository = screenwriterRepository;
    }

    public Screenwriter addScreenwriter(Screenwriter screenwriter) {
        return screenwriterRepository.save(screenwriter);
    }
    @Transactional
    public Optional<Screenwriter> getScreenwriter(int id) {
        Optional<Screenwriter> screenwriter = screenwriterRepository.findById(id);
        return screenwriter;
    }
    public List<Screenwriter> getAllScreenwriters() {
        return screenwriterRepository.findAll();
    }
}

