fetch('http://localhost:8080/actors')
    .then(response => response.json())
    .then(actors => {
        const list = document.getElementById('actorsList');
        actors.forEach(actor => {
            const item = document.createElement('li');
            const link = document.createElement('a');
            link.href = `actor.html?id=${actor.id}`;
            link.textContent = actor.name;
            item.appendChild(link);
            list.appendChild(item);
        });
    })
    .catch(error => console.error('Error:', error));