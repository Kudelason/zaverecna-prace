package cz.vsb.fei.java.projekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "cz.vsb.fei.java.projekt.api.repository") // Set your repository package if needed
@EntityScan("cz.vsb.fei.java.projekt.api.model") // Set your model package if needed
public class ProjektApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProjektApplication.class, args);
    }
}
