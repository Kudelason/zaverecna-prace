package cz.vsb.fei.java.projekt.api.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Screenwriter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column()
    private String name;

    @Column()
    private String nationality;

    @OneToMany(mappedBy = "screenwriter", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Movie> movies = new ArrayList<>();

    @Column()
    private String birthDate;

    public Screenwriter(String name, String nationality, String birthDate) {
        this.name = name;
        this.nationality = nationality;
        this.birthDate = birthDate;
        this.movies = new ArrayList<>();
    }

    public void addMovie(Movie movie) {
        movies.add(movie);
        movie.setScreenwriter(this);
    }
}

