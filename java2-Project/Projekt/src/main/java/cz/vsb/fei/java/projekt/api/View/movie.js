const params = new URLSearchParams(window.location.search);
const movieId = params.get('id');
var title;

fetch(`http://localhost:8080/movies/${movieId}`)
    .then(response => response.json())
    .then(movie => {
        const details = document.getElementById('movieDetails');
        details.innerHTML = `<h2>${movie.title} (${movie.releaseYear})</h2>
                                <p><strong>Description:</strong> ${movie.description}</p>`;
        title = movie.title;

        return fetch('http://localhost:8080/directors');
    })
    .then(response => response.json())
    .then(directors => {
        const directorDetails = document.getElementById('director');
        const foundDirector = directors.find(director => director.movies.some(m => m.title === title));
        if (foundDirector) {
            const link = document.createElement('a');
            link.href = `director.html?id=${foundDirector.id}`;
            link.textContent = `Director: ${foundDirector.name}`;
            directorDetails.appendChild(link);
        } else {
            directorDetails.innerHTML = `<p>No director found for this movie.</p>`;
        }

        return fetch('http://localhost:8080/screenwriters');
    })
    .then(response => response.json())
    .then(screenwriters => {
        const screenwriterDetails = document.getElementById('screenwriter');
        const foundScreenwriter = screenwriters.find(screenwriter => screenwriter.movies.some(m => m.title === title));
        if (foundScreenwriter) {
            const link = document.createElement('a');
            link.href = `screenwriter.html?id=${foundScreenwriter.id}`;
            link.textContent = `Screenwriter: ${foundScreenwriter.name}`;
            screenwriterDetails.appendChild(link);
        } else {
            screenwriterDetails.innerHTML = `<p>No screenwriter found for this movie.</p>`;
        }

        return fetch('http://localhost:8080/actors');
    })
    .then(response => response.json())
    .then(actors => {
        const actorsDetails = document.getElementById('actors');
        const foundActors = actors.filter(actor => actor.movies.some(m => m.title === title));
        if (foundActors.length > 0) {
            actorsDetails.innerHTML = `<h3>Actors:</h3>`;
            foundActors.forEach(actor => {
                const link = document.createElement('a');
                link.href = `actor.html?id=${actor.id}`;
                link.textContent = actor.name;
                const item = document.createElement('p');
                item.appendChild(link);
                actorsDetails.appendChild(item);
            });
        } else {
            actorsDetails.innerHTML = `<p>No actors found for this movie.</p>`;
        }
    })
    .catch(error => console.error('Error:', error));
