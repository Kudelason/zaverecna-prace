package cz.vsb.fei.java.projekt.api.repository;
import cz.vsb.fei.java.projekt.api.model.Director;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DirectorRepository extends JpaRepository<Director, Integer> {
}

