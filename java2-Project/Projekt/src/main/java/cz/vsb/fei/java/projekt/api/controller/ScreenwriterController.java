package cz.vsb.fei.java.projekt.api.controller;

import cz.vsb.fei.java.projekt.api.model.Director;
import cz.vsb.fei.java.projekt.api.model.Screenwriter;
import cz.vsb.fei.java.projekt.service.ScreenwriterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/screenwriters")
public class ScreenwriterController {

    private final ScreenwriterService screenwriterService;

    public ScreenwriterController(ScreenwriterService screenwriterService) {
        this.screenwriterService = screenwriterService;
    }

    @CrossOrigin(origins = "*")
    @GetMapping
    public List<Screenwriter> getAllScreenwriters() {
        return screenwriterService.getAllScreenwriters();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{id}") // Using PathVariable for better REST practices
    public ResponseEntity<Screenwriter> getScreenwriter(@PathVariable int id) {
        return screenwriterService.getScreenwriter(id)
                .map(ResponseEntity::ok) // If found, return 200 OK with the movie
                .orElseGet(() -> ResponseEntity.notFound().build()); // If not found, return 404 Not Found
    }
}
