const params = new URLSearchParams(window.location.search);
const actorId = params.get('id');

fetch(`http://localhost:8080/actors/${actorId}`)
    .then(response => response.json())
    .then(actor => {
        const actorDetails = document.getElementById('actorDetails');
        actorDetails.innerHTML = `<h2>${actor.name}</h2>
                                    <p><strong>Birth Date:</strong> ${new Date(actor.birthDate).toLocaleDateString()}</p>`;

        const moviesList = document.getElementById('moviesList');
        if (actor.movies.length > 0) {
            moviesList.innerHTML = '<h3>Movies Featuring:</h3>';
            actor.movies.forEach(movie => {
                const item = document.createElement('div');
                const link = document.createElement('a');
                link.href = `movie.html?id=${movie.id}`;
                link.textContent = `${movie.title} (${movie.releaseYear})`;
                item.appendChild(link);
                item.innerHTML += `<p>${movie.description}</p>`;
                moviesList.appendChild(item);
            });
        } else {
            moviesList.innerHTML = `<p>No movies found for this actor.</p>`;
        }
    })
    .catch(error => console.error('Error:', error));