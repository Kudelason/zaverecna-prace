package cz.vsb.fei.java.projekt.service;

import cz.vsb.fei.java.projekt.api.model.Actor;
import cz.vsb.fei.java.projekt.api.model.Director;
import cz.vsb.fei.java.projekt.api.repository.ActorRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ActorService {

    private final ActorRepository actorRepository;

    public ActorService(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }
    public List<Actor> getAllActors() {
        return actorRepository.findAll();
    }

    @Transactional
    public Optional<Actor> getActor(int id) {
        Optional<Actor> actor = actorRepository.findById(id);
        return actor;
    }
}
