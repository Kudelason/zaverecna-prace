fetch('http://localhost:8080/movies')
    .then(response => response.json())
    .then(movies => {
        const list = document.getElementById('moviesList');
        movies.forEach(movie => {
            const item = document.createElement('li');
            const link = document.createElement('a');
            link.href = `movie.html?id=${movie.id}`;
            link.textContent = movie.title;
            item.appendChild(link);
            list.appendChild(item);
        });
    })
    .catch(error => console.error('Error:', error));