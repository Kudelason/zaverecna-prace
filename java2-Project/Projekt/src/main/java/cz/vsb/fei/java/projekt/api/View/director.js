const params = new URLSearchParams(window.location.search);
const directorId = params.get('id');

fetch(`http://localhost:8080/directors/${directorId}`)
    .then(response => response.json())
    .then(director => {
        const directorDetails = document.getElementById('directorDetails');
        directorDetails.innerHTML = `<h2>${director.name}</h2>
                                        <p><strong>Nationality:</strong> ${director.nationality}</p>
                                        <p><strong>Birth Date:</strong> ${new Date(director.birthDate).toLocaleDateString()}</p>`;

        const moviesList = document.getElementById('moviesList');
        if (director.movies.length > 0) {
            moviesList.innerHTML = '<h3>Movies Directed:</h3>';
            director.movies.forEach(movie => {
                const item = document.createElement('div');
                const link = document.createElement('a');
                link.href = `movie.html?id=${movie.id}`;
                link.textContent = `${movie.title} (${movie.releaseYear})`;
                item.appendChild(link);
                item.innerHTML += `<p>${movie.description}</p>`;
                moviesList.appendChild(item);
            });
        } else {
            moviesList.innerHTML = `<p>No movies found for this director.</p>`;
        }
    })
    .catch(error => console.error('Error:', error));