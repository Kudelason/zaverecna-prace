package cz.vsb.fei.java.projekt.api.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Director {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)  // Ensure name is always provided
    private String name;

    @Column()
    private String nationality;

    @OneToMany(mappedBy = "director", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Movie> movies = new ArrayList<>();

    @Column()
    private String birthDate;

    public Director(String name, String nationality, String birthDate) {
        this.name = name;
        this.nationality = nationality;
        this.birthDate = birthDate;
        this.movies = new ArrayList<>();
    }

    // Method to add a movie to the list
    public void addMovie(Movie movie) {
        movies.add(movie);
        movie.setDirector(this);
    }
}
