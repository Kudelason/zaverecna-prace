package cz.vsb.fei.java.projekt.api.repository;

import cz.vsb.fei.java.projekt.api.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActorRepository extends JpaRepository<Actor, Integer> {
}
