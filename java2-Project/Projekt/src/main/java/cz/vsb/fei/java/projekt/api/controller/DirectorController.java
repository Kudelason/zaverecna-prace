package cz.vsb.fei.java.projekt.api.controller;

import cz.vsb.fei.java.projekt.api.model.Director;
import cz.vsb.fei.java.projekt.api.model.Movie;
import cz.vsb.fei.java.projekt.service.DirectorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/directors")
public class DirectorController {

    private final DirectorService directorService;

    public DirectorController(DirectorService directorService) {
        this.directorService = directorService;
    }
    @CrossOrigin(origins = "*")
    @GetMapping
    public List<Director> getAllDirectors() {
        return directorService.getAllDirectors();
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}") // Using PathVariable for better REST practices
    public ResponseEntity<Director> getDirector(@PathVariable int id) {
        return directorService.getDirector(id)
                .map(ResponseEntity::ok) // If found, return 200 OK with the movie
                .orElseGet(() -> ResponseEntity.notFound().build()); // If not found, return 404 Not Found
    }
}
