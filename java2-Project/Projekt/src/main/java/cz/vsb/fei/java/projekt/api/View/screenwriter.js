const params = new URLSearchParams(window.location.search);
const screenwriterId = params.get('id');

fetch(`http://localhost:8080/screenwriters/${screenwriterId}`)
    .then(response => response.json())
    .then(screenwriter => {
        const screenwriterDetails = document.getElementById('screenwriterDetails');
        screenwriterDetails.innerHTML = `<h2>${screenwriter.name}</h2>
                                            <p><strong>Nationality:</strong> ${screenwriter.nationality}</p>
                                            <p><strong>Birth Date:</strong> ${new Date(screenwriter.birthDate).toLocaleDateString()}</p>`;

        const moviesList = document.getElementById('moviesList');
        if (screenwriter.movies.length > 0) {
            moviesList.innerHTML = '<h3>Movies Written:</h3>';
            screenwriter.movies.forEach(movie => {
                const item = document.createElement('div');
                const link = document.createElement('a');
                link.href = `movie.html?id=${movie.id}`;
                link.textContent = `${movie.title} (${movie.releaseYear})`;
                item.appendChild(link);
                item.innerHTML += `<p>${movie.description}</p>`;
                moviesList.appendChild(item);
            });
        } else {
            moviesList.innerHTML = `<p>No movies found for this screenwriter.</p>`;
        }
    })
    .catch(error => console.error('Error:', error));