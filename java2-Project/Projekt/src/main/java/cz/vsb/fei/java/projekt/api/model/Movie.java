package cz.vsb.fei.java.projekt.api.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private int releaseYear;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "director_id")
    @JsonBackReference
    private Director director;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "screenwriter_id")
    @JsonBackReference
    private Screenwriter screenwriter;

    @ManyToMany
    @JsonBackReference
    @JoinTable(
            name = "movie_actor",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "actor_id")
    )
    private Set<Actor> actors = new HashSet<>();

    public Movie(String title, String description, int year, Director director, Screenwriter screenwriter) {
        this.title = title;
        this.description = description;
        this.releaseYear = year;
        this.director = director;
        this.screenwriter = screenwriter;
    }
}
