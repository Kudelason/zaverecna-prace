# Web Application README

## Introduction

This repository contains the source code for a client-server web application
 designed to function as a basic movie database, similar in concept to IMDb.
 The application is developed using Java Spring for the backend, featuring
 a RESTful interface, and a dynamic frontend. It interacts with a database
 through the Java Persistence API (JPA), enabling robust
 data management and retrieval.

## Features

- **Movie Listings:**

 Users can browse an extensive list of movies,
  viewing detailed information about each title.

- **Individual Profiles:**

 Actors, directors, and screenwriters have their own detailed profiles,
  which include lists of movies they have
  worked on and their roles in those productions.

- **Search and Filter:**

 The platform offers search functionality,
  allowing users to find movies or personnel
  based on specific criteria such as name,
  genre, or release year.

- **Interactive Interface:**

 The frontend is built to provide an
  interactive user experience, facilitating easy navigation
  through the vast content related
  to movies and movie-making personnel.

This application aims to provide a central hub
 for movie enthusiasts to explore and learn more
 about their favorite films and the people behind them,
 offering insights into the diverse world of cinema.

## Prerequisites

Before running the application, ensure you have the following installed:

- Java Development Kit (JDK)

## Installation

- Clone the repository:

   ```
   git clone https://github.com/kudelason/zaverecna-prace.git
   ```

## Usage

### Run the backend server

- Navigate to the backend directory:

   ```
   cd java2-Project/Projekt
   ```

- Run the application:

   ```
   mwn exec:java
   ```

### Run frontend application command

- Open default browser:

   ```
   xdg-open java2-Project/Projekt/src/main/java/cz/vsb/fei/java/projekt/api/View/movies.html
   ```

## Contributing

If you would like to contribute to the project, please follow these steps:

- Fork the repository
- Create your feature branch (git checkout -b feature/YourFeature)
- Commit your changes (git commit -am 'Add some feature')
- Push to the branch (git push origin feature/YourFeature)
- Create a new Pull Request

## License

- Licensed under the MIT License.

## Contact

- For any inquiries or support, feel free to contact me.
